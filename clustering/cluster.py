import pandas as pd
from sklearn.cluster import KMeans, AgglomerativeClustering, DBSCAN, OPTICS, SpectralClustering
from sklearn.metrics import silhouette_score
from sklearn.manifold import TSNE, MDS, Isomap
import pickle
import matplotlib.pyplot as plt
import numpy as np


def cluster_silhouette(data, max_k, method=KMeans):
    scores = []
    ks = range(2, max_k)

    for k in ks:
        print(k)
        model = method(n_clusters=k)
        labels = model.fit_predict(X=data)
        scores.append(silhouette_score(data, labels))

    plt.figure()
    plt.plot(ks, scores)


def plot_tsne(df, **kwargs):
    tsne = TSNE(**kwargs)
    X_tsne = tsne.fit_transform(df.values)
    plt.figure()
    plt.scatter(X_tsne[:, 0], X_tsne[:, 1])
    return tsne.kl_divergence_


def plot_clusters(df, k):
    _, ax_tsne = plt.subplots()
    counts = df['cluster'].value_counts()

    for i in range(df['cluster'].min(), df['cluster'].max() + 1):
        cluster_data = df[df['cluster'] == i]
        aantallen = (cluster_data != 0).transpose().sum(axis=1).drop(
            labels=['cluster', 'tsne_x', 'tsne_y']).sort_values(ascending=False).iloc[0:20]
        aantallen = aantallen[aantallen > 0]
        print(aantallen)
        fig, ax = plt.subplots()
        aantallen.plot(kind='barh', ax=ax)
        ax.set_title('%r' % (i))
        fig.tight_layout()
        ax.set_xlabel('Aantal indicaties')
        ax_tsne.scatter(cluster_data['tsne_x'], cluster_data['tsne_y'])

#
# data = pd.read_csv('../dataset/samengevoegd2.csv', sep=';')
# hc_filters = ['J - ']
# sc_filters = ['Overige producten (ovp)', 'Vervoer', 'Consultatie', 'Diagnostiek']
#
# data = data[~(data['Hoofdcategorie'].isin(hc_filters)) & ~(data['Subcategorie'].isin(sc_filters))]
# data['Einddatum_zorg'] = pd.to_datetime(data['Einddatum_zorg']) + pd.DateOffset(days=1)
# data['Begindatum_zorg'] = pd.to_datetime(data['Begindatum_zorg'])
#
# data['duur'] = data['Einddatum_zorg'] - data['Begindatum_zorg']
# data['duur'] = pd.to_numeric(data['duur'])
# data['ones'] = 1

data_roermond = pd.read_excel('../dataset/301.xlsx')
data_roermond = data_roermond[data_roermond['Product'] != 'Vervoer']
data_roermond['Einddatum'] = pd.to_datetime(data_roermond['Einddatum']) + pd.DateOffset(days=1)
data_roermond['Begindatum'] = pd.to_datetime(data_roermond['Begindatum'])

data_roermond['duur'] = data_roermond['Einddatum'] - data_roermond['Begindatum']
data_roermond['duur'] = pd.to_numeric(data_roermond['duur'])
print(list(data_roermond))

features = 'Product'

pivot = data_roermond.dropna(subset=[features]).pivot(values='duur', columns=features).fillna(0)
pivot = pivot.apply(lambda col: col / col[col != 0].mean())

# pivot['bsn'] = data['HASH_ID_KP']
pivot['bsn'] = data_roermond['BSN_hash']
pivot = pivot.groupby('bsn').sum()

sample = pivot.sample(frac=1)
categories = list(sample)
sample = sample.reset_index()
sample_filtered = sample
X_filtered = sample_filtered[categories].values
# cluster_silhouette(X_filtered, 10, KMeans)
#
k = 6

model = AgglomerativeClustering(n_clusters=k)
X_tsne = TSNE(perplexity=30, n_jobs=-1, verbose=1).fit_transform(X_filtered)
sample_filtered['tsne_x'] = X_tsne[:, 0]
sample_filtered['tsne_y'] = X_tsne[:, 1]

model.fit(X=sample_filtered[['tsne_x', 'tsne_y']].values)

sample_filtered['cluster'] = model.fit_predict(X=sample_filtered[['tsne_x', 'tsne_y']].values)
print(list(sample_filtered))
sample_filtered[['bsn', 'cluster', 'tsne_x', 'tsne_y']].to_csv('clusters.csv')
print(list(sample_filtered))
plot_clusters(sample_filtered, k)

plt.show()
