library(shiny)
library(data.table)
library(timevis)
library(tidyverse)
library(scales)
library(DescTools)
library(reshape2)
library(networkD3)
library(RColorBrewer)
library(viridis)
library(DT)
library(plotly)

source('read_data.R')

declaraties <- get_declaraties()
indicaties <- get_indicaties()
# UI layout voor zorgpaden dashboard
fluidPage(
  
  titlePanel("Zorgpaden"),
  
  # De app bestaat uit 7 tabbladen:
  # 1. Alarmbellen: indicatoren die aanleiding tot verder onderzoek kunnen geven. Per client en zorgaanbieder
  # 2. Client: informatie per client. Indicaties en declaraties op tijdlijn, statistieken en zorgprofiel
  # 3. Zorgaanbieder: informatie per zorgaanbieder. Aantal clienten, indicaties, marktaandeel per categorie, volume per categorie, reden beeindiging, doorstroom.
  # 4. Verwijzer: informatie per verwijzer. In ontwikkeling
  # 5. Doorstroom: sankey diagrommen voor de productcategorie, product, zorgaanbieder
  # 6. Clusters: weergave van client clustering.
  # 7. Algemene toelichting.
  mainPanel(
    tabsetPanel(type = "tabs",
                tabPanel("Alarmbellen",
                         wellPanel(
                           selectInput(inputId = 'varSelect', label='Selecteer perspectief', choices=c("Client"=0, "Zorgaanbieder"=1), selected='Client'),
                           selectInput(inputId = "zwaarSelectie", label="Zware producten:", choices=sort(unique(indicaties$Product)), multiple = T)
                         ),
                         DT::DTOutput(outputId = "alarmbellen")),
                tabPanel("Client",
                         wellPanel(
                           selectInput(inputId = "BSN", label="Client", choices=sort(unique(indicaties$BSN)), selectize=TRUE),
                           checkboxInput(inputId="checkboxDeclaratiesAan", label = "Declaraties", value = TRUE)
                         ),
                         timevisOutput(outputId = "timeline"),
                         plotlyOutput(outputId = "zorgProfiel")),
                tabPanel("Zorgaanbieder",
                         wellPanel(selectInput(inputId = "AGB", label="Aanbieder", choices=sort(unique(indicaties$AGB_code)), selectize=TRUE)),
                         htmlOutput('info'),
                         tableOutput('marktAandeel'),
                         tableOutput('categorieVolume'),
                         plotlyOutput(outputId = "zaProfiel"),
                         plotlyOutput(outputId = "redenBeeindiging"),
                         selectInput(inputId = 'sankeySelect', label='Selecteer variabele', choices=c("Zorgaanbieder"=0, "Productcategorie"=1), selected='Zorgaanbieder'),
                         sliderInput(inputId = 'sankeyCutoff', label = 'Mininum aantal indicaties', min=1, max=25, value = 1),
                         sankeyNetworkOutput(outputId = 'sankeyZa')),
                tabPanel("Verwijzer",
                         wellPanel(selectInput(inputId = "verwijzerSelectie", label="verwijzer", choices=sort(unique(indicaties$VerwijzerID)))),
                         tableOutput('verwijzer')),
                tabPanel("Productcategorie",
                         wellPanel(selectInput(inputId = "productenSelectie", label="Productcategorie", choices=sort(unique(indicaties$Productcategorie)))),
                         tableOutput('Productenverdeling'),
                         plotlyOutput('zorgaanbiederVerdeling')),
                tabPanel("Doorstroom",
                         wellPanel(selectInput(inputId = "sankeySelectie", label="Perspectief", choices=c('Productcategorie'=1, 'Product'=2, 'Zorgaanbieder'=3))),
                         sliderInput(inputId = 'sankeyCutoffTotaal', label = 'Mininum aantal indicaties', min=1, max=200, value = 1),
                         sankeyNetworkOutput(outputId = 'sankeyTotaal')),
                tabPanel("Clusters",
                         plotOutput('tsne'),
                         selectInput('clusterSelect', label='Selecteer cluster', choices = 0:5),
                         plotOutput('clusterDuiding')),
                tabPanel("Algemene toelichting",
                         includeMarkdown('Zorgpaden_beschrijving.md'))
                
    )
  )
)