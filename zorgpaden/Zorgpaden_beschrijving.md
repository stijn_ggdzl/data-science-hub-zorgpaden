# **ZORGPADEN**

#
## Algemene toelichting

ALGEMENE OPMERKINGEN:

- De Twentse dataset heeft betrekking op de Jeugdhulp periode in 2015 tot en met 2018
- De Roermondse dataset heeft betrekking op de Jeugdhulp in de periode januari 2018 tot en met juli 2019

GEHANTEERDE DEFINITIES:

Indicatie:
Een indicatie is een aanwijzing dat een bepaalde ondersteuningsbehoefte (jeugdhulp) gedurende een bepaalde periode moet of kan worden gegeven aan een cliënt. De ondersteuningsbehoefte kan betrekking hebben op de cliënt zelf maar ook op het (pleeg)gezin(systeem) waar de cliënt deel van uitmaakt.

Cliënt:
Een natuurlijk persoon in de leeftijd van 0 tot 28 jaar die gebruik kan of moet maken van Jeugdhulp.

Verlenging:
Cliënt krijgt binnen 6 weken na beëindiging zorgproduct hetzelfde zorgproduct bij dezelfde zorgaanbieder opnieuw toegewezen.

Herindicatie:
Cliënt krijgt binnen 6 weken na beëindiging zorgproduct:

- hetzelfde zorgproduct bij een andere zorgaanbieder toegewezen, of;
- Een ander zorgproduct bij dezelfde zorgaanbieder toegewezen, of;
- Een ander zorgproduct bij een andere zorgaanbieder toegewezen.

Recidive:
Cliënt krijgt niet eerder dan 6 weken maar niet later dan 6 maanden na beëindiging zorgproduct een nieuwe indicatie voor hetzelfde of een ander zorgproduct bij dezelfde of een andere zorgaanbieder.

Nieuw zorgtraject:
Cliënt krijgt 6 maanden (of later) na beëindiging zorgproduct een nieuwe indicatie voor hetzelfde of een ander zorgproduct bij dezelfde of een andere zorgaanbieder.

Toelichting per tabblad

### **Tabblad Alarmbellen**

AGB-code (_AGB\_code_):
De AGB-code is de unieke sleutel waarmee zorgaanbieders worden geregistreerd in het landelijke AGB-register. In dit register staat alle noodzakelijke (zorg) informatie vermeld om het declareren, de zorginkoop, het contracteren en het &#39;gidsen&#39; om de zorg mogelijk te maken.

Gemiddeld aantal aanbieders per cliënt (_aantalAanbieders\_gem_)_:_
Dit is het aantal aanbieders dat een cliënt bij deze aanbieder in een periode van 90 dagen gemiddeld heeft.

Gemiddelde aantal verblijfsproducten per cliënt (_verblijfGelijk\_gem_):
Gemiddeld aantal indicaties voor verblijfsproducten dat een cliënt bij deze zorgaanbieder heeft gehad wanneer de betreffende cliënt tenminste één of meer van dergelijke indicaties heeft.

Gemiddelde aantal crisisproducten per cliënt (_crisisGelijk\_gem_):
Gemiddeld aantal indicaties voor crisisvoorzieningen dat een cliënt bij deze zorgaanbieder heeft gehad wanneer de betreffende cliënt tenminste één of meer van dergelijke indicaties heeft.

Aantal Clienten (_aantalClienten_):
Het aantal cliënten dat deze zorggaanbieder in zorg heeft.

### **Tabblad Cliënt**

Tijdbalk:
Aan de rechterzijde geeft de tijdbalk weer voor welke zorgproducten bij welke aanbieders de geselecteerde cliënt een zorgindicatie heeft gehad en wanneer op die zorgindicatie declaraties zijn ontvangen. De blauw gearceerde balken geven de periodes van de zorgindicaties weer en de rood gearceerde balken de periodes van de declaraties. Wanneer een deel van een balk een donkere kleur heeft is er sprake van overlap (van indicaties dan wel declaraties).

Taartdiagram:
In het taartdiagram rechtsonder wordt voor de geselecteerde cliënt per productcategorie de aandelen van de zorgindicaties in de totale zorgperiode weergegeven.
In de cirkel rondom de taartdiagram wordt het aantal uren in de indicatiesperiode weergegeven. Daarbij geldt dat er 24 uur in een dag zitten.

Deze cirkel aanpassen en daarin het aantal dagen opnemen. Let erop dat niet het nulpunt (0) maar het totale aantal dagen worden weergegeven.

### **Tabblad Zorgaanbieder**

**Eerste tabel:**

Productcategorie: Groepering van zorgproducten in 12 (dataset Twente) of 15 (dataset Roermond) categorieën.

Marktaandeel: (_Marktaandeel_):
Het marktaandeel van de geselecteerde zorgaanbieder in de betreffende productcategorie gemeten in het aantal indicaties van het totaal in de dataset. Daarbij is bijvoorbeeld 0.33 gelijk aan 33%.

Gemiddeld verschil tussen de indicatiebesluit en de start van de indicatie (_GemiddeldVerschilStartIndicatie_):
Dit is het gemiddelde tijdsduur in dagen tussen de dag dat het indicatiebesluit is afgegeven en de dag dat de indicatie start. Bij een negatief getal lijkt het erop dat de indicatie eerder start (geldig is) dan dat het indicatiebesluit is afgegeven. De indicatie is dan met terugwerkende kract afgegeven.

Gemiddeld verschil tussen de startdatum indicatie en de eerste dag van de eerste declaratie. (_gemiddeldVerschilDeclaratieStart_)
Dit is het gemiddelde tijdsduur in dagen tussen de dag dat de indicatie start en de eerste dag van de declaratieperiode van de eerste declaratie. Dit is een indicatieve maat voor de wachttijd van een cliënt voordat de zorgaanbieder start met de zorgverlening.

Aandeel van herindicaties in het totaal aantal indicaties (_AandeelHerindicaties_):
Dit is het aandeel (in %) van de herindicaties (verlengingen) in het totaal aantal indicaties van de geselecteerde zorgaanbieder in de betreffende productcategorie.

Deze maatstaf moet nog worden gesplitst in:

Aandeel van de verlengingen in het totaal aantal indicaties (_AandeelVerlengingen_):
Dit is het aandeel (in %) van de verlengingen in het totaal aantal indicaties van de geselecteerde zorgaanbieder in de betreffende productcategorie

Aandeel van de herindicerende indicaties in het totaal aantal indicaties  (_AandeelHerindicaties_):
Dit is het aandeel (in %) van de indicaties in het totaal aantal indicaties van de geselecteerde zorgaanbieder in de betreffende productcategorie die een herindicatie betreffen.

Aandeel van de indicaties die leiden tot recidive in het totaal aantal indicaties (_AandeelRecidive_):
Dit is het aandeel (in %) van de indicaties in het totaal aantal indicaties van de geselecteerde zorgaanbieder in de betreffende productcategorie die na afloop van de indicatie leiden tot recidive van cliënten.

**Tweede tabel:**

Productcategorie: Groepering van zorgproducten in 12 (dataset Twente)of 15 (dataset Roermond) categorieën.

Eenheid volume (_Eenheid_):
Dit is de volume-eenheid van de producten in productcategorie. Het kan voorkomen dat een productcategorie twee of meer keer met verschillende volume-eenheden staat vermeld. Dat wordt verklaard door het feit dat éénzelfde product in verschillende indicaties met verschillende volume-eenheden is toegewezen.

Gemiddelde volume (_Gemiddelde\_volume_):
Dit is de gemiddelde omvang van het volume van de indicaties in de betreffende productcategorie.

Gemiddeld aantal zorgdagen (_Gem\_aantal\_zorgdagen_):
Dit is de gemiddelde lengte van de indicatiesperiode in dagen.

Taartdiagram:
In het taartdiagram wordt voor de geselecteerde zorgaanbieder per productcategorie de aandelen van de zorgindicaties in de totale zorgduur van alle zorgindicaties weergegeven.
In de cirkel rondom de taartdiagram wordt het totale aantal zorguren in de indicatiesperiodes van alle indicaties van de geselecteerde zorgaanbieder weergegeven. Daarbij geldt dat er 24 uur in een dag zitten.

Sankeydiagram met selectievariabele &quot;Zorgaanbieder&quot;:
In Sankeydiagram wordt voor de geselecteerde zorgaanbieder, op grond van de indicaties, weergegeven welke de voorgaande zorgaanbieders waren en welke de opvolgende zorgaanbieders waren. Door met de cursor op de grijs gearceerde &#39;stromen&#39; te gaan staan, kan het aantal indicaties worden afgelezen. Met het schuifje &quot;minimum&quot; kan het minimum aantal indicaties worden geselecteerd, dat moet worden weergegeven.

Sankeydiagram met selectievariabele &quot;Productcategorie&quot;:
In Sankeydiagram wordt voor de geselecteerde zorgaanbieder, op grond van de indicaties, weergegeven welke de voorgaande productcategorieën waren en welke de opvolgende productcategorieën waren. Door met de cursor op de grijs gearceerde &#39;stromen&#39; te gaan staan, kan het aantal indicaties worden afgelezen. Met het schuifje &quot;minimum&quot; kan het minimum aantal indicaties worden geselecteerd, dat moet worden weergegeven.